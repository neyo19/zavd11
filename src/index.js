import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import {MainReducer} from "./redux/reducers/MainReducer";
import App from './App';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Basket from "./components/Basket/Basket";

const store = createStore(MainReducer ,   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

ReactDOM.render(

    <React.StrictMode>
        <Provider store={store}>
            <Header/>
            <Basket/>
            <App />
            <Footer/>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

