import React, {useState} from "react";
import CardGroup from 'react-bootstrap/CardGroup'
import Card from 'react-bootstrap/Card'
import './Product.css'
import {Accordion} from "react-bootstrap";
import CloseButton from "react-bootstrap/CloseButton";
import {useDispatch} from "react-redux";


export default function Product({cart, onAdd, onDel , cartView,itemCount}) {

    const [compoundPrice, setCompoundPrice] = useState(null);
    const [count, setCount] = useState(1);
    const [cartCount, setcartCount] = useState(itemCount);
    const dispatch = useDispatch();

    const onChange = (v, target) => {
        setCount(v);
        setCompoundPrice(cart.price * v);
        if (v > 200){
            alert("You can't buy more than 200 pcs");
            setCompoundPrice(v = null);
            setCount(1);
            target.value = '';
        }else if(v <= 1){
            setCompoundPrice(v = null);
            setCount(1);
            target.value = '';
        }
    }
    function onAdd() {
        dispatch({type: "ADD_PRODUCT" , payload: {count: + count , product: cart}})
    }

    return (
        <>
            <CardGroup id = 'CardGroup'>
                <Card  id = 'Card'>
                    <Card.Img id='img' variant="top" src={cart.image} />
                    <Card.Body  style = {{background: '#F4F3E9'}}>
                        <Card.Title id ='text'>{cart.title}</Card.Title>
                    </Card.Body>
                    <Card.Footer style = {{background: '#F4F3E9'}}>
                        {cartView === "add"?
                            <>
                            <div id = 'd1'> <p id = 'ln1'> How Much ? </p>
                                <input type="number"
                                       pattern="^[ 0-9]+$"
                                       className='inp'
                                       value={count}
                                       onChange={(e) => onChange(e.target.value, e.target)}/>
                                <button id="btn-on-cart" onClick={onAdd}> To cart </button>
                                </div>
                                <div>
                                <Accordion id='Description'>
                                    <Accordion.Item eventKey="0" >
                                        <Accordion.Header >   Description:   </Accordion.Header>
                                        <Accordion.Body className='Desc'>
                                            {cart.description}
                                        </Accordion.Body>
                                    </Accordion.Item>
                                </Accordion>

                            </div>
                            </>
                            : cartView === "delete"?
                                <div>  <h5> How Much ? </h5>
                                    <input type="number"
                                           pattern="^[ 0-9]+$"
                                           className='inp'
                                           value={itemCount}
                                           onChange={(e) => onChange(e.target.value, e.target)}/>
                                    <button
                                        id="btn-on-cart"
                                        onClick={onDel}>
                                        <CloseButton variant="black"/>
                                    </button>
                                    <Accordion id='Description'>
                                        <Accordion.Item eventKey="0" >
                                            <Accordion.Header >   Description:   </Accordion.Header>
                                            <Accordion.Body className='Desc'>
                                                {cart.description}
                                            </Accordion.Body>
                                        </Accordion.Item>
                                    </Accordion>
                                </div> : null
                        }
                        <p id ='text'> {Math.ceil(!compoundPrice ? cart.price : compoundPrice)} $ </p>
                    </Card.Footer>
                </Card>
            </CardGroup>
        </>
    );
};

