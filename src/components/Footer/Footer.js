import React from 'react';
import './Footer.css'


function Footer() {

    const year = new Date();

    return (

        <>
            <div className='footer'>
                <div className='left-footer'>
                    <p id='footer-time'> © 2001 - {year.getFullYear()} NEYO </p>
                </div>

                <div className='center-footer'>
                    <p id='light-text'> Navigation </p>
                    <p> Basket  </p>



                </div>

                <div className='right-footer'>
                    <p id='light-text'> Contact Us </p>
                    <p>   <a id='a' href='https://twitter.com/?lang=ru'>   Twitter    </a> </p>
                    <p>   <a id='a' href='https://uk-ua.facebook.com'>   FaceBook    </a> </p>
                    <p>   <a id='a' href='https://www.instagram.com'>   Instagram    </a> </p>


                </div>
            </div>
        </>
    );
}

export default Footer;
