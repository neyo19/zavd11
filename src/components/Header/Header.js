import React  from "react";
import Navbar from 'react-bootstrap/Navbar';
import {Nav} from "react-bootstrap";
import {useDispatch} from "react-redux";
import './Header.css';


function Header() {
    const handleShow = () => dispatch({type: 'CART_OPEN'});
    const dispatch = useDispatch();


    return (
        <>
            <div className="header">
                <Navbar id='navbar' collapseOnSelect expand="lg">
                    <Navbar.Brand id="br"> NEYO  </Navbar.Brand>
                    <Navbar.Toggle id='navtoggle' aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse  id="responsive-navbar-nav">
                        <Nav className="me-auto">
                            <div className='button-bg'>
                                <button className="buttonGroup" onClick={handleShow}> Busket </button>

                            </div>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        </>
    );

}

export default Header;