import CardGroup from "react-bootstrap/CardGroup";
import Offcanvas from "react-bootstrap/Offcanvas";
import CloseButton from "react-bootstrap/CloseButton";
import Product from "../Product/Product";
import React  from "react";
import {useDispatch, useSelector} from "react-redux";

function Basket(){

    const cart = useSelector(state => state.cart);
    const dispatch = useDispatch();
    const show = useSelector(state => state.cartOpened)

    const cartClose = () => {
        dispatch({type: 'CART_CLOSE'})
    }

    return(
<>
        <CardGroup id = 'CardP'>
            <Offcanvas id="topmodal"  show={show}   placement='start' >
                <Offcanvas.Header>
                    <Offcanvas.Title id = 'topmodal'>  Selected  {cart.length > 0? <h9>
                            : {cart.length}  </h9>:
                        false } </Offcanvas.Title>
                    <button id='close'
                            onClick={cartClose}>
                        <CloseButton variant="black"/>
                    </button>
                </Offcanvas.Header>
                <Offcanvas.Body id="Modal" >
                    { cart.length ? cart.map((el, id) => {
                            return (
                                <div key={id}>
                                    <hr/>
                                    <Product
                                        cart={el.product}
                                        itemCount={el.count}
                                        cartView={"delete"}
                                        onDel={()=>{dispatch({type: "DEL_PRODUCT",
                                            payload: {product: el.product}})}}/>

                                </div>
                            )
                        })
                        :

                        <p id='text' >  </p>
                    }

                </Offcanvas.Body>
                {cart.length>0?<div>
                        <button className='del-button'
                                onClick={()=>{dispatch({type: "DEL_ALL_PRODUCT",
                                    payload: {}})}}>
                            Remove All
                        </button>
                        <button id='buy'> Buy </button> </div>:
                    <div id='buy'>   <p id='text-card-bottom'> Your basket is Empty Now</p> </div> }
            </Offcanvas>
        </CardGroup>
</>
    );
}

export default Basket