import './App.css';
import React, {useEffect, useState} from "react";
import axios from "axios";
import Spinner from 'react-bootstrap/Spinner'
import Product from "./components/Product/Product";
import CardGroup from 'react-bootstrap/CardGroup'
import {useDispatch} from "react-redux";


function App() {

    const [productData, setProductData] = useState([]);
    const dispatch = useDispatch();
    const URL = `https://fakestoreapi.com/products`;

    useEffect(() => {
        axios({url: `${URL}`})
            .then((response) => {
                    setProductData(response.data);
                }
            ).catch((err) => {
            window.alert(`${err} cant load Goods from ${URL}`)
        })

    }, [URL]);

    return (


            <CardGroup id = 'CardP'>
                { productData.length ? productData.map((el , id) => {
                        return <Product
                            key={id}
                            cart={el}
                            cartView={'add'}
                          />;
                    })
                    :
                    <div id='load'>  <Spinner className='load-spinner'  animation="border"/> </div>
                }
            </CardGroup>

    );
}

export default App;
